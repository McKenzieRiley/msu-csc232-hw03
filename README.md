# Homework #3
## Working with Grammars in C++

## Due: 23:59 Friday 01 October 2016

## Points: 5

This homework problem is a realization of Exercise 6, pages 184-185 of your textbook, which states:

Consider a language of words, where each word is a string of dots and dashes. The following grammar describes this language in particular:

```
<word> = <dot>|<dash><word>|<word><dot>
<dot>  = .
<dash> = -
```

In this homework assignment, you are to implement the `isWord()` operation of the `WordParser` class:

```
/**
 * Determine whether a given word matches the language described above.
 *
 * @param s the string under interrogation
 * @return True is returned if the given string matches the language; false otherwise.
 */
 bool WordParser::isWord(std::string s);
```

You know you have succeeded when all the tests found in the `HomeworkDriver.cpp` file pass. There are two `main()` methods defined in this file. You can toggle the value of the `FIDDLING` macro to select which main method you wish to run. However, *before your final commit* be sure to set this macro value to `FALSE` so that my autograder grades the correct output. **Failure to do so may result in a poor grade**.

To compile and run the unit tests (which require that `FIDDLING` is set to `FALSE`), one would execute the following command:

```
$ g++ -std=c++11 *.cpp -lcppunit
```

(Don't type the `$` -- that's the command line prompt.)
If you have coded your solution correctly, the output will look like:

```
WordParserTest::testBaseCase
 : OK
WordParserTest::testDotDotDot
 : OK
WordParserTest::testDotDotDash
 : OK
WordParserTest::testDotDashDot
 : OK
WordParserTest::testDotDashDash
 : OK
WordParserTest::testDashDotDot
 : OK
WordParserTest::testDashDotDash
 : OK
WordParserTest::testDashDashDot
 : OK
WordParserTest::testDashDashDash
 : OK
WordParserTest::testFourDotTwoDash
 : OK
WordParserTest::testSevenCharMoreDashThanDots
 : OK
OK (11)
```

Note, "straight out of the box" (i.e., without coding anything on your part), the output will appear as:

```
WordParserTest::testBaseCase
 : assertion
WordParserTest::testDotDotDot
 : assertion
WordParserTest::testDotDotDash
 : OK
WordParserTest::testDotDashDot
 : OK
WordParserTest::testDotDashDash
 : OK
WordParserTest::testDashDotDot
 : assertion
WordParserTest::testDashDotDash
 : OK
WordParserTest::testDashDashDot
 : assertion
WordParserTest::testDashDashDash
 : OK
WordParserTest::testFourDotTwoDash
 : OK
WordParserTest::testSevenCharMoreDashThanDots
 : assertion
WordParserTest.cpp:47:Assertion
Test name: WordParserTest::testBaseCase
equality assertion failed
- Expected: 1
- Actual  : 0
- actual should equal expected

WordParserTest.cpp:55:Assertion
Test name: WordParserTest::testDotDotDot
equality assertion failed
- Expected: 1
- Actual  : 0
- actual should equal expected

WordParserTest.cpp:87:Assertion
Test name: WordParserTest::testDashDotDot
equality assertion failed
- Expected: 1
- Actual  : 0
- actual should equal expected

WordParserTest.cpp:103:Assertion
Test name: WordParserTest::testDashDashDot
equality assertion failed
- Expected: 1
- Actual  : 0
- actual should equal expected

WordParserTest.cpp:127:Assertion
Test name: WordParserTest::testSevenCharMoreDashThanDots
equality assertion failed
- Expected: 1
- Actual  : 0
- actual should equal expected

Failures !!!
Run: 11   Failure total: 5   Failures: 5   Errors: 0
```

Don't let the passing tests fool you... these are false negatives: Test(s) that are marked as passed when in reality they have failed or there is some problem in functionality or there is a bug.

## To Do...

1. Fork https://bitbucket.org/professordaehn/msu-csc232-hw03 into a private repo in your Bitbucket account as per our standard operating procedure.
2. Checkout your forked repo and create a develop branch to do your work.
3. Replace <Your Name> on line 3 of WordParser.cpp with your name (and be sure to remove the brackets too). Commit this change using the commit message: "CSC232-HW3 - Added name to header comments."
4. Implement the `isWord()` method using recursion. Commit as often as you see fit.
5. When you have fully implemented your solution, create a pull request to merge your develop branch into the master branch on your private repo. Any pull requests made on my original repo will be declined and you will receive no credit for this assignment.
Log onto Blackboard and navigate to Content > Homework to find the link to submit this assignment for grading. Submit the assignment by entering the URL of your pull request -- as a working hyperlink -- in the Text Submission field of the assignment. It is the timestamp on this last step that dictates whether the assignment as been submitted on time or not. That is, regardless of when the commits were made to your repo, the assignment is considered late if not submitted on Blackboard by the due date

If you have *any* questions, do not hesitate to call, text, video chat, etc.

## Grading

1. (1 point) Pull Request: Did the student
    1. properly fork my repo into a private repo in their account?
    1. create a develop branch within which to do the work?
    1. create a pull request on their private repo?
    1. submit assignment to Blackboard on time?
1. (1 point) Comment Quality: Did the student
    1. use proper grammar -- including spelling -- in their comments?
    1. write comments that fully specify each operation?
    1. use the appropriate Doxygen tags (e.g., `@param`, `@pre`, `@post`, etc. See Appendix I) to support the documentation?
1. (3 points) Implementation: Did the student
    1. implement the `isWord()` opertion using recursion?
    1. make a commit with an appropriate commit message upon the completion of this specification?
    1. pass all the tests?
