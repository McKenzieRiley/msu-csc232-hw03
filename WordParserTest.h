/*
 * @file WordParserTest.h
 * @author James R. Daehn
 * @brief WordParser unit test specification. DO NOT MODIFY THIS FILE! DOING SO
 * WILL RESULT IN A FAILING GRADE OF 0.
 */

#ifndef WORDPARSERTEST_H_
#define WORDPARSERTEST_H_

#include <cppunit/extensions/HelperMacros.h>
#include "WordParser.h"

class WordParserTest : public CPPUNIT_NS::TestFixture  {
	CPPUNIT_TEST_SUITE(WordParserTest);
		CPPUNIT_TEST(testBaseCase);
		CPPUNIT_TEST(testDotDotDot);
		CPPUNIT_TEST(testDotDotDash);
		CPPUNIT_TEST(testDotDashDot);
		CPPUNIT_TEST(testDotDashDash);
		CPPUNIT_TEST(testDashDotDot);
		CPPUNIT_TEST(testDashDotDash);
		CPPUNIT_TEST(testDashDashDot);
		CPPUNIT_TEST(testDashDashDash);
		CPPUNIT_TEST(testFourDotTwoDash);
		CPPUNIT_TEST(testSevenCharMoreDashThanDots);
	CPPUNIT_TEST_SUITE_END();

private:
	static const std::string BASE_CASE;
	static const std::string DOT_DOT_DOT;
	static const std::string DOT_DOT_DASH;
	static const std::string DOT_DASH_DOT;
	static const std::string DOT_DASH_DASH;
	static const std::string DASH_DOT_DOT;
	static const std::string DASH_DOT_DASH;
	static const std::string DASH_DASH_DOT;
	static const std::string DASH_DASH_DASH;
	static const std::string FOUR_DOT_TWO_DASH;
	static const std::string SEVEN_CHAR_MORE_DASH_THAN_DOTS;
	static const std::string TEST_ASSERTION_FAILURE_MESSAGE;

	WordParser wordParser;
public:
	WordParserTest();
	virtual ~WordParserTest();
	void setUp();
	void tearDown();
	void testBaseCase();
	void testDotDotDot();
	void testDotDotDash();
	void testDotDashDot();
	void testDotDashDash();
	void testDashDotDot();
	void testDashDotDash();
	void testDashDashDot();
	void testDashDashDash();
	void testFourDotTwoDash();
	void testSevenCharMoreDashThanDots();
};

#endif /* WORDPARSERTEST_H_ */
